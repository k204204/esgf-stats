import os
import smtplib


from email.mime.base import MIMEBase
from email import Encoders


def send_mail(message, subject, to_mail):
    import smtplib
    from email.mime.multipart import MIMEMultipart
    # send only mail in case logfile is not empty
    from_mail = 'esgf@dkrz.de'
    msg = MIMEMultipart()
    msg['Subject'] = subject
    msg['From'] = from_mail
    msg['To'] = to_mail
    to_mail_list = to_mail.split(',')
    msg.set_content(message)

    server = smtplib.SMTP('mailhost.dkrz.de', 25)
    server.starttls()
    server.sendmail(from_mail, to_mail_list, msg.as_string())
    server.quit()
