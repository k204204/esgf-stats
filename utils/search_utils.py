def get_search_context(conn, **constraints):
    return conn.new_context(constraints)
