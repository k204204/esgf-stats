# !/usr/bin/env python

from setuptools import setup, find_packages

VERSION = 0.1

setup(
    name='esgf-stats',
    version=VERSION,
    description='ESGF Stats',
    author='Katharina Berger',
    author_email='berger@dkrz.de',
    install_requires=[
        "esgf-pyclient>=0.1.8",
        "tabulate>=0.8.6",
    ],
    packages=find_packages(),
    include_package_data=True,
    scripts=[
        'scripts/esgfstats',
    ],
    zip_safe=False,  # Migration repository must be a directory
)
