from pyesgf.search import SearchConnection
from tabulate import tabulate
import datetime
import urllib2
import ssl

suffixes = ['B', 'KB', 'MB', 'GB', 'TB', 'PB']

def get_human_size(nbytes):
    int(nbytes)
    if nbytes == 0:
        return '0 B'
    i = 0
    while nbytes >= 1024 and i < len(suffixes)-1:
        nbytes /= 1024.
        i += 1
    f = ('%.2f' % nbytes).rstrip('0').rstrip('.')
    return '%s %s' % (f, suffixes[i])


def get_search_context(conn, constraints):
    return conn.new_context(**constraints)


def return_sum_for_facet(ctx, facet):
    sum = 0
    for num, count in ctx.facet_counts[facet].iteritems():
        sum += int(int(num) * int(count))
    return sum


def pretty_print_table(headers, value_table):
    return str(tabulate(value_table, headers=headers, tablefmt='orgtbl'))


def get_project_and_size(conn, data_nodes, stats_dict, project_dict, constraints):
    res = []
    for dn in data_nodes:
        constraints['facets'] = 'project'
        constraints['data_node'] = dn
        if 'project' in constraints:
            del constraints['project']
        if 'retracted' in constraints:
            del constraints['retracted']
        ctx = get_search_context(conn, constraints)
        projects = ctx.facet_counts['project'].keys()
        for project in projects:
            if 'project' in constraints:
                del constraints['project']
            if 'retracted' in constraints:
                del constraints['retracted']
            constraints['project'] = project
            constraints['facets'] = 'size,number_of_files'
            ctx = get_search_context(conn, constraints)

            size_sum = return_sum_for_facet(ctx, 'size')
            number_of_files_sum = return_sum_for_facet(ctx, 'number_of_files')
            number_of_datasets_sum = ctx.hit_count

            # get stats for retracted data
            constraints['retracted'] = True
            ctx = get_search_context(conn, constraints)
            number_of_datasets_retracted = ctx.hit_count

            size_retracted = return_sum_for_facet(ctx, 'size')
            number_of_files_retracted = return_sum_for_facet(ctx, 'number_of_files')
            size_sum -= size_retracted
            if size_sum > 0:
                number_of_files = number_of_files_sum - number_of_files_retracted
                number_of_datasets = number_of_datasets_sum - number_of_datasets_retracted
                if project_dict is not None:
                    if project in project_dict:
                        project_dict[project][0] += number_of_datasets  # number_of_datasets
                        project_dict[project][1] += number_of_files     # number_of_files
                        project_dict[project][2] += size_sum    # size
                    else:
                        project_dict[project] = [number_of_datasets, number_of_files, size_sum]

                # if dn not in res:
                #     res[dn] = []

                stats_dict['number_of_datasets'] += number_of_datasets
                stats_dict['number_of_files'] += number_of_files
                stats_dict['size'] += size_sum

                res.append([dn,
                            project,
                            number_of_datasets,
                            # number_of_datasets_retracted,
                            number_of_files,
                            # number_of_files_retracted,
                            get_human_size(size_sum),
                            # get_human_size(size_retracted)
                            ])

    # for dn in res:
        # headers = ['Project', 'Number Datasets', 'Number Datasets Retracted', 'Number Files', 'Number Files Retracted', 'Size', 'Size Retracted']
        # headers = ['Data Node', 'Project', 'Number Datasets', 'Number Files', 'Size']
        # # parse_list = res[dn][0:1, 3, 5]
        # # parse_list.insert(0, dn)
        # table = pretty_print_table(headers, res[dn])
        # print table
    return res, stats_dict, project_dict


def get_local_stats(search_base_url, local_stats):
    msg = ''

    conn = SearchConnection(search_base_url, distrib=False)
    constraints = {'facets': 'data_node'}
    ctx = get_search_context(conn, constraints)
    data_nodes_local = ctx.facet_counts['data_node'].keys()

    # get list of all DKRZ data nodes
    data_nodes_dkrz = [dn for dn in data_nodes_local if 'dkrz.de' in dn]

    local_stats['number_of_dkrz_datanodes'] = len(data_nodes_dkrz)
    local_stats['number_of_extern_datanodes'] = len(data_nodes_local)-len(data_nodes_dkrz)

    # get all "original" published data per project (retracted=false)
    constraints['replica'] = False
    result, local_stats, _ = get_project_and_size(conn, data_nodes_dkrz, local_stats, None, constraints)
    if result:
        msg += 'DKRZ data nodes, original data:\n\n'
        headers = ['Data Node', 'Project', 'Number Datasets', 'Number Files', 'Size']
        msg += pretty_print_table(headers, result)

    # get all "replica" published data (retracted=false)
    constraints['replica'] = True
    result, local_stats, _ = get_project_and_size(conn, data_nodes_dkrz, local_stats, None, constraints)
    if result:
        msg += '\n\nDKRZ data nodes, replicated data:\n\n'
        headers = ['Data Node', 'Project', 'Number Datasets', 'Number Files', 'Size']
        msg += pretty_print_table(headers, result)

    # list of external nodes publising to DKRZ index
    data_nodes_ext = set(data_nodes_local).difference(set(data_nodes_dkrz))
    # get all "original" published data per project (retracted=false)
    constraints['replica'] = False
    result, local_stats, _ = get_project_and_size(conn, data_nodes_ext, local_stats, None, constraints)
    if result:
        msg += '\n\nDKRZ index, original data:\n\n'
        headers = ['Data Node', 'Project', 'Number Datasets', 'Number Files', 'Size']
        msg += pretty_print_table(headers, result)

    # get all "replica" published data (retracted=false)
    constraints['replica'] = True
    result, local_stats, _ = get_project_and_size(conn, data_nodes_ext, local_stats, None, constraints)
    if result:
        msg += '\n\nDKRZ index, replica data:\n\n'
        headers = ['Data Node', 'Project', 'Number Datasets', 'Number Files', 'Size']
        msg += pretty_print_table(headers, result)

    msg += '\n\nNumber of Datasets (all Projects): %s\n' % local_stats['number_of_datasets']
    msg += 'Number of Files (all Projects): %s\n' % local_stats['number_of_files']
    msg += 'Size (all Projects): %s\n' % get_human_size(local_stats['size'])
    msg += 'Number of DKRZ Data Nodes: %s\n' % local_stats['number_of_dkrz_datanodes']
    msg += 'Number of external Data Nodes publishing to DKRZ Index: %s\n' % local_stats['number_of_extern_datanodes']

    return msg, local_stats


def get_global_stats(search_base_url, global_stats):
    msg = ''
    msg_tmp = ''

    # all ESGF nodes but data nodes publishing to DKRZ index
    # conn = SearchConnection(search_base_url, distrib=False)
    # constraints = {'facets': 'data_node'}
    # ctx = get_search_context(conn, constraints)
    # data_nodes_local = ctx.facet_counts['data_node'].keys()

    conn = SearchConnection(search_base_url, distrib=True)
    constraints = {'facets': 'data_node,index_node'}
    ctx = get_search_context(conn, constraints)
    datanodes_all = ctx.facet_counts['data_node'].keys()
    indexnodes_all = ctx.facet_counts['index_node'].keys()
    # datanodes_global = set(datanodes_all).difference(set(data_nodes_local))

    global_stats['number_of_indexnodes'] = len(indexnodes_all)
    global_stats['number_of_datanodes'] = len(datanodes_all)

    # get all "original" published data per project (retracted=false)
    constraints['replica'] = False
    result, global_stats, project_dict = get_project_and_size(conn, datanodes_all, global_stats, {}, constraints)

    project_list = []
    for project in project_dict:
        project_list.append([str(project)] + project_dict[project])
    if project_list:
        msg += '\n\nESGF wide, original data, per project:\n\n'
        headers = ['Project', 'Number Datasets', 'Number Files', 'Size']
        msg += pretty_print_table(headers, project_list)

    if result:
        msg_tmp += '\n\nESGF wide, original data:\n\n'
        headers = ['Data Node', 'Project', 'Number Datasets', 'Number Files', 'Size']
        msg_tmp += pretty_print_table(headers, result)

    # get all "replica" published data (retracted=false)
    constraints['replica'] = True
    result, global_stats, project_dict = get_project_and_size(conn, datanodes_all, global_stats, {}, constraints)

    project_list = []
    for project in project_dict:
        project_list.append([project] + project_dict[project])
    if project_list:
        msg += '\n\nESGF wide, replica data, per project:\n\n'
        headers = ['Project', 'Number Datasets', 'Number Files', 'Size']
        msg += pretty_print_table(headers, project_list)

    if result:
        msg_tmp += '\n\nESGF wide, replica data:\n\n'
        headers = ['Data Node', 'Project', 'Number Datasets', 'Number Files', 'Size']
        msg_tmp += pretty_print_table(headers, result)

    msg += '\n\nNumber of Datasets (all Projects): %s\n' % global_stats['number_of_datasets']
    msg += 'Number of Files (all Projects): %s\n' % global_stats['number_of_files']
    msg += 'Size (all Projects): %s\n' % get_human_size(global_stats['size'])
    msg += 'Number of Index Node: %s\n' % global_stats['number_of_indexnodes']
    msg += 'Number of Data Nodes: %s\n' % global_stats['number_of_datanodes']

    return msg, msg_tmp, global_stats


def get_download_stats():

    # get unix time for first of current month and first of previous month
    cur_month = (datetime.date.today().replace(day=1))
    month = '{:02d}'.format(int(cur_month.strftime("%m")) - 1)
    year = int(cur_month.strftime("%Y"))
    if int(month) == 0:
        month = 12
        year = int(cur_month.strftime("%Y")) - 1
    data_nodes = ['esgf1.dkrz.de', 'esgf2.dkrz.de', 'esgf3.dkrz.de']
    out_list = []

    context = ssl._create_unverified_context()

    stats_month = '%s-%s-01' % (year, month)
    for data_node in data_nodes:
        if data_node == 'esgf1.dkrz.de':
            url = 'https://%s/api/stats' % data_node
        else:
            url = 'https://%s/stats' % data_node
        stats = urllib2.urlopen(url, context=context)
        lines = stats.read().split()
        for line in lines:
            date, project, downloads, users, size = line.split(',')[0:]
            downloads = int(downloads)
            users = int(users)
            size = float(size)
            if str(date) == str(stats_month):
                if '%' in project:
                    project = project.split('%')[1]
                    out_list.append([data_node, project, users, downloads, get_human_size(size)])
    headers = ['Data Node', 'Project', 'Number of Users', 'Number of Downloads', 'Downloaded Size']
    return stats_month, pretty_print_table(headers, out_list)

